import numpy as np
import pandas as pd
import settings

from datetime import datetime
from projeto.utils.utils import objEncode, fix_null
from projeto.utils.Model import objModel

from projeto.constants import Message
from projeto.restplus import objLogger


def classifier(arg):
    data = {key: value for key, value in arg.items() if key in settings.CATEGORICAL}
    data = objEncode.transform(fix_null(data))
    arg.update(data)
    arg = pd.DataFrame({key: [value] for key, value in arg.items()})
    arg["predict"] = objModel.predict_classifier(arg.values)
    arg = {key: value[0] for key, value in arg.to_dict("list").items()}
    return arg

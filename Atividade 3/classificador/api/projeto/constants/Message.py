ERROR_GENERIC = 'Ops... Não sei o que acontecei, mas estamos trabalhando para corrigir. Tente novamente ou mais tarde.'
ERROR_NOT_TREATMENT = 'Ocorreu algo que não esperavamos e para seu conforto estaremos olhando.'
ERROR_NOT_FOUND = 'Opsss... acredito que o servidor está fora do ar.'
ERROR_NONE = 'Poxa esqueci de tratar o valor nulo e ocasionei um problema para você.'
ERROR_EXCEPTION = 'Não tratado.'

REQUEST = "Request recebida."

# Debug Business #
DEBUG_B64_DECODE = "Debug: Decoded file to base64."
DEBUG_B64_ENCODE = "Debug: Encoded file to base64."
DEBUG_CLASSIFYING = "Debug: Classificando a imagem..."
DEBUG_CLASSIFIED = "Debug: Classificado como: {}."
DEBUG_LOAD_MODEL = "Debug: Carregando modelo Keras."
DEBUG_PREDICT = "Debug: Predict do modelo."
DEBUG_ROTATE = "Debug: Rotating image..."
DEBUG_RESIZE = "Debug: resizing image..."
DEBUG_FFT = "Debug: FFT image..."
DEBUG_ANG = "Debug: Find angle amount: {}, final angle: {}."
DEBUG_TIMESTAMP = "Debug: Atividade: {}  Inicio: {}\t Termino: {}\t Tempo: {}"


# Error Business #
ERROR_BO = 'Exception: business rule error and something unexpected occurred.'
ERROR_LOAD_MODEL = "Debug: Falha no carregamento do modelo."
ERROR_DIVISION = 'Exception: error div for zero.'
ERROR_NO_REQUEST_DATA = "Exception: TypeError, No request data."
ERROR_NO_KEY_REQUEST = "Exception: KeyError, wrong key found on request data."
ERROR_BASE64 = "Exception: binascii.Error, Base64 was broken."
ERROR_IMAGE_CONVERT = "Exception: ImageConvertError, file is not in Allowed files extensions."
ERROR_CLASSIFICATION = "Exception: ClassificationException, file not classified as expected."

# Sucess Business #
SUCESS_EXEMPLO = "Success: done!"  # Example
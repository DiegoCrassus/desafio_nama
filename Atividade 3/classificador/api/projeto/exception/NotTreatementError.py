from projeto.constants import Message


class NotTreatmentException(Exception):

    def __init__(sel):
        super().__init__(Message.ERROR_NOT_TREATMENT)

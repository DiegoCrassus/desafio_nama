# from builtins import ZeroDivisionError
from projeto.constants import Message


class ClassificationException(Exception):

    def __init__(sel):
        super().__init__(Message.ERROR_CLASSIFICATION)

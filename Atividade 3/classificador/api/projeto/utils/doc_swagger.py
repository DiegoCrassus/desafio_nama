from flask_restplus import fields
from projeto.restplus import api
                                               
OUTPUT_DATA = api.model('Output',
                        {'messages': fields.String(required=True,
                                                   description='Sucess or Error mensage'),
                         'status': fields.String(required=True,
                                                 description='Status code'),
                         'data': fields.Raw(required=False,
                                            description="list of dict. Ex: [{'label': 'lucelia', 'image_b64':'iVBORw0...'}]")})

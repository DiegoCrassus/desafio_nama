import numpy as np
import pandas as pd
import math
import settings
import warnings

from glob import glob
from pickle import load
from projeto.constants import Message
from projeto.restplus import objLogger

warnings.filterwarnings("ignore")


def fix_null(data):
    arg = pd.DataFrame({key: [value] for key, value in data.items()})
    for x in arg:
        if "NA" in arg[x].unique():
            arg[x] = arg[arg[x] != "NA"]
            arg[x] = np.where(arg[x].isnull(),"Unknown", arg[x])

    return arg


class Encode:
    def __init__(self):
        weights = [x for x in glob(settings.MODEL_WEIGHT + "*") if "lb" in x]
        name_model = [x.split("_")[-1].split(".")[0] for x in weights]
        print("Encoders found: {}".format(name_model))
        self.models = {key: load(open(value, "rb")) for key, value in zip(name_model, weights)}

    def transform(self, arg):
        response = {}
        for key, value in arg.items():
            model = self.models[key]
            response[key] = model.transform([value])[0]

        return response

objEncode = Encode()
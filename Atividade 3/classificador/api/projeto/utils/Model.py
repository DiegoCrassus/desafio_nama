import settings
import tensorflow as tf

from loguru import logger
from projeto.constants import Message


class ModelClassifier:
    def __init__(self):
        tf.keras.backend.clear_session()

        config = tf.compat.v1.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.compat.v1.Session(config=config)

    def predict_classifier(self, arg):
        '''
            Método estático para realizar o predict do modelo Keras.
        '''
        # set_session(self.sess)
        try:
            with self.sess.graph.as_default():
                logger.debug(settings.MODEL_WEIGHT + "atividade_keras.h5")
                
                model = tf.keras.models.load_model(settings.MODEL_WEIGHT + "atividade_keras.h5")
                predict = model.predict(arg)
                if predict[0][0] > 0.5:
                    return 1

                else:
                    return 0
        except OSError:
            logger.error("[-] --- {}".format(Message.ERROR_LOAD_MODEL))
            return

objModel = ModelClassifier()
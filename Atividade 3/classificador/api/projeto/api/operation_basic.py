import logging
import settings
from base64 import binascii
from flask import request
from flask_restplus import Resource
from projeto.restplus import api
from projeto.constants import CodeHttp, Message
from projeto.utils import doc_swagger
from projeto.restplus import objLogger, objResponse
from projeto.bo.classificador import classifier
from projeto.exception.ClassificationError import ClassificationException

log = logging.getLogger(__name__)
ns = api.namespace(settings.ENDPOINTS, description='Post operação.')


@ns.route(settings.ROUTE_CLASSIFICADOR)
class PostsCollection(Resource):

    @api.response(200, 'Enviado com sucesso.')
    @api.marshal_with(doc_swagger.OUTPUT_DATA)
    def post(self):
        """
        Método POST
        """
        objLogger.debug(Message.REQUEST)
        request_data = request.get_json()
        response = []

        try:
            if len(request_data) == settings.INPUT_SHAPE:
                response = classifier(request_data)

            if not response:
                raise ClassificationException

        except KeyError as error:
            response = objResponse.send_exception(objError=error, messages=Message.ERROR_NO_KEY_REQUEST, status=CodeHttp.ERROR_500)
            objLogger.error(messages=Message.ERROR_NO_KEY_REQUEST)

        except ClassificationException as error:
            response = objResponse.send_exception(objError=error, messages=Message.ERROR_CLASSIFICATION, status=CodeHttp.ERROR_500)
            objLogger.error(messages=Message.ERROR_CLASSIFICATION)

        except TypeError as error:
            response = objResponse.send_exception(objError=error, messages=Message.ERROR_NO_REQUEST_DATA, status=CodeHttp.ERROR_500)
            objLogger.error(messages=Message.ERROR_NO_REQUEST_DATA)

        except binascii.Error as error:
            response = objResponse.send_exception(objError=error, messages=Message.ERROR_BASE64, status=CodeHttp.ERROR_500)
            objLogger.error(messages=Message.ERROR_BASE64)

        else:
            response = objResponse.send_success(messages=Message.SUCESS_EXEMPLO, status=CodeHttp.SUCCESS_200, data=response)
            objLogger.success(messages=Message.SUCESS_EXEMPLO)

        return response

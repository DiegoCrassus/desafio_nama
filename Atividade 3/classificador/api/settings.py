import os

"""
    Variaveis booleanas e objetos nao poderao ser definidas nas variaveis de ambiente,
    pois todas serao convertidas para string.
    Para as variaveis definidas com "os.environ.get()" o primeiro valor é referente
    a variavel que está buscando, o segundo valor será usado como valor padrão caso
    não encontre nas variaveis de ambiente.
"""

# flask settings
FLASK_DEBUG = True  # Do not use debug mode in production
FLASK_SERVER_NAME = None
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = list
RESTPLUS_ERROR_404_HELP = False

# Env_vars
FLASK_HOST = os.environ.get('FLASK_HOST', "0.0.0.0")
FLASK_PORT = os.environ.get('FLASK_PORT', "5000")

FLASK_URL_FIX = os.environ.get("FLASK_URL_FIX", "/api")
ROUTE_CLASSIFICADOR = os.environ.get("ROUTE_CLASSIFICADOR", "/classificador")
ENDPOINTS = os.environ.get("ENDPOINTS", "endpoint")

PATH_LOG = os.environ.get("PATH_LOG", "./log_classificador")
CATEGORICAL = os.environ.get("CATEGORICAL", "v12,v55,v42,v33,v76,v68,v7,v70,v32,v28,v99,v95,v85,v84,v44").split(",")

MODEL_WEIGHT = os.environ.get("DIR_MODEL_WEIGHT", "./projeto/model/")
INPUT_SHAPE = int(os.environ.get("INPUT_SHAPE", "21"))

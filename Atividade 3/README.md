# Atividade 3

Api de classificação que utiliza os pesos do modelo feito na Atividade 1.

## Funcionamento

Execute `docker-compose up -d` para inicialização da api utilziando container, a api irá iniciar no link `http://localhost:5000/api/` onde dará acesso ao swagger da api.
A api espera um POST na url `http://localhost:5000/api/endpoint/classificador` com o body similar ao exemplo a seguir:

```
{
    "v33": "v",
    "v76": "g",
    "v12": "ff",
    "v68": "f",
    "v50": 1.75,
    "v7": "v",
    "v70": "t",
    "v55": "v",
    "v20": 5.4e-05,
    "v24": 1,
    "v32": "v",
    "v97": 2.82,
    "v28": "t",
    "v99": "v",
    "v95": "v",
    "v42": "v",
    "v53": 5,
    "v85": "v",
    "v9": 0,
    "v84": "f",
    "v44": "f"
}
``` 
